<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\payment;

class gateController extends Controller
{
    //

    public function user_submit()
    {
        // return "this is ok";

        $data = array(
            'marchant_id' => '12345',
            'amount' => '500',
            'reference' => '567',
        );

        return view('gateway')->with('data', $data);
    }

    public function receive_post(Request $request)
    {
        $data = array(
            'marchant_id' => $request->merchant_id,
            'amount' => $request->amount,
            'reference' => $request->reference,
        );

        return view('gateway')->with('data', $data);

    }

    public function to_merchant_gateway(Request $request)
    {
        $result = $request->all();

       // print_r($result);

        if($request->marchant=="Stripe" or $request->marchant=="visa"){
        
           return view("stripe")->with('result', $result);
        }
       
     return view('bank')->with('result', $result);

    }

    public function report(Request $request){

       $result =payment::all();

     //   print_r($result);
     echo "<table border='1' class='table'>";
     echo "<tr>";
            echo "<td> marchant_id</td>";
            echo "<td> amount</td>";
            echo "<td> marchant_id</td>";
            echo "<td> bank Name</td>";
            echo "<td> Gate way Name</td>";
            echo "<td> Pay Date</td>";
            echo "</tr>";
        foreach( $result as $value){

            echo "<tr>";
            echo "<td> ".$value->marchant_id."</td>";
            echo "<td> ".$value->amount."</td>";
            echo "<td> ".$value->reference."</td>";
            echo "<td> ".$value->bank."</td>";
            echo "<td> ".$value->gateway."</td>";
            echo "<td> ".$value->created_at."</td>";
            echo "</tr>";
        }

        echo "</table>";

        return "this is report";
    }

}
