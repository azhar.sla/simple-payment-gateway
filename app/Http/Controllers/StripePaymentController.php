<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Session;
use Stripe;
use App\payment;
   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 500 * 500,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment for green" 
        ]);

        
        $result=$request->all();
        
        print_r($result);
        $payment=new payment;
        $payment->marchant_id=$request->marchant_id;
        $payment->amount=$request->amount;
        $payment->reference=$request->reference;
        $payment->gateway=$request->marchant;
        $payment->bank=$request->marchant;
        
        $payment->save();
        dd('pay ment is successful');
  
        Session::flash('success', 'Payment successful!');
          
        return back();
    }
}