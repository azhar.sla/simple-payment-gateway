<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   
</head>
<body>
  
<div class="container">

<div class="row">
		<div class="card">
			<div class="card-header">
				<h3>Enter user name and password to procees</h3>
			</div>
			<div class="card-body">
				<form action="{{ route('bank_gateway') }}" method="post">

                @csrf

                <input type='hidden' name='marchant_id' value='{{$_POST['marchant_id']}}' >
                        <input type='hidden' name='amount' value='{{$_POST['amount']}}' >
                        <input type='hidden' name='reference' value='{{$_POST['reference']}}' >
                        <input type='hidden' name='marchant' value='{{$_POST['marchant']}}' >

					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user">username</i></span>
						</div>
						<input type="text" class="form-control" placeholder="adam" name="name" required>
						
					</div>

					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key">password</i></span>
						</div>
						<input type="password" class="form-control" placeholder="adam123" name="password" required>
					</div>

					<div class="row align-items-center remember">
						<input type="checkbox">Remember Me
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="#">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div>
				<div class="d-flex justify-content-center">
				<h4>Use below for Demo Purpose</h4>
					<p>UserName : adam</p>
					<p>password : adam123</p>
				</div>
			</div>
		</div>
	</div>
</div>

</body>

</html>