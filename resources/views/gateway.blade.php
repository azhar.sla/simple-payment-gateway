<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
</head>
<body>
  
<div class="container">
 user marchat id is :{{ $data['marchant_id']}} <br>
 user pay amount  is :{{ $data['amount']}} <br>
 user reference  is :{{ $data['reference']}} <br>
 
    <div class="row">

    <form action="{{ route('report')}}" method="POST">
        @csrf
        <input type="submit" value="submit" name="submit">

    </form>
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                       <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Select Payment Plan</h3>
                        <div class="display-td" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                     
                </div>
                <div class="panel-body">
  
                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
  
                    <form role="form" action="{{ route('merchant_gateway') }}" method="post" class="require-validation"
                                                  
                                                    id="payment-form">
                        @csrf
                     
                        <input type='hidden' name='marchant_id' value='{{$data['marchant_id']}}' >
                        <input type='hidden' name='amount' value='{{$data['amount']}}' >
                        <input type='hidden' name='reference' value='{{$data['reference']}}' >

  
                        <div class='form-row row'>
                            <div class='col-xs-4 form-group card required'>
                                <label class='control-label'>PayPal</label>  <input
                                    autocomplete='off' class='form-control card-number' size='10'
                                    type='radio' name='marchant' value='paypal'>
                            </div>
                            <div class='col-xs-4 form-group card required'>
                                <label class='control-label'>Stripe</label> <input
                                    autocomplete='off' class='form-control card-number' size='10'
                                    type='radio' name='marchant' value='Stripe'>
                            </div>
                            <div class='col-xs-4 form-group card required'>
                                <label class='control-label'>ipay</label> <input
                                   class='form-control radio' size='10'
                                    type='radio' name='marchant' value='ipay'>

                            </div>
                        </div>

                        <div class='form-row row' >
                            <div class='col-xs-5 form-group card required' >
                            <label class='control-label'>VISA/MasterCard/American Express/Unio Pay</label> <input
                                   class='form-control radio' size='10'
                                    type='radio' name='marchant' value='visa'>
                                 
                            </div>

                            <div class="col-xs-7 form-group card required" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                            
                        </div>

                        <div class='form-row row' >
                            <div class='col-xs-12 form-group card required' >
                              <br>  <h3 style='background-color:gray;'>Pay with Bank Account</h3> 
                            </div>
                            
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-3 form-group required'>
                               MayBank2u  <BR>  <input
                                 type='radio' name='marchant' value='maybank'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               PublicBank  <BR> <input
                                 type='radio' name='marchant' value='publicbank'>
                            </div>
                            <div class='col-xs-3 form-group required' >
                               HongLongBank <input
                                 type='radio' name='marchant' value='honglong'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               CIMB CLICK <BR> <input
                                 type='radio' name='marchant' value='cimb'>
                            </div>
                        </div>


                        <div class='form-row row'>
                            <div class='col-xs-3 form-group required'>
                               AFFIN BANK  <BR>  <input
                                 type='radio' name='marchant' value='affin'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               AM BANK  <BR> <input
                                 type='radio' name='marchant' value='ambank'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               ANOTHER <BR> <input
                                 type='radio' name='marchant' value=''>
                            </div>
                            <div class='col-xs-3 form-group required'>
                            ANOTHER <BR> <input
                                 type='radio' name='marchant' value=''>
                            </div>


                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-3 form-group required'>
                               AFFIN BANK  <BR>  <input
                                 type='radio' name='marchant'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               AM BANK  <BR> <input
                                 type='radio' name='marchant'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                               ANOTHER <BR> <input
                                 type='radio' name='marchant'>
                            </div>
                            <div class='col-xs-3 form-group required'>
                            ANOTHER <BR> <input
                                 type='radio' name='marchant'>
                            </div>


                        </div>
  
                       
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
  
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now </button>
                            </div>
                        </div>
                          
                    </form>
                </div>
            </div>        
        </div>
    </div>
      
</div>
  
</body>
  

</html>