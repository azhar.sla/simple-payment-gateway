<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/payment',function(){

    return view('stripe');
})->name('stripe');


Route::get('/gateway', 'gateController@user_submit');
Route::post('/gateway', 'gateController@to_merchant_gateway')->name('merchant_gateway');

Route::post('/bank', 'bankController@to_bank_gateway')->name('bank_gateway');

Route::get('/bank',function(){

    return view('bank');
})->name('bankgate');

Route::post('/report', 'gateController@report')->name('report');